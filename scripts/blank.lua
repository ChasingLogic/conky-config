-- Conky.lua
--
require 'cairo'

function conky_main()

    -- SETUP STUFF

    if conky_window == nil then return end
    local cs = cairo_xlib_surface_create(conky_window.display, conky_window.drawable, conky_window.visual, conky_window.width, conky_window.height)
    cr = cairo_create(cs)
    local updates=tonumber(conky_parse('${updates}'))

    -- ACTUAL STUFF

    if updates > 5 then

        --DO STUFF

    end --if updates > 5

    -- CLEANUP
    
    cairo_destroy(cr)
    cairo_surface_destroy(cs)
    cr=nil

end --end main function
